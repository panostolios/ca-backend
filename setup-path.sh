#!/bin/bash
export PIPENV_VERBOSITY=-1
export VENV_PATH=`pipenv --venv`/bin
echo Adding virtual env path \'$VENV_PATH\' to PATH
export PATH=$VENV_PATH:$PATH

