from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin


User = get_user_model()


class CustomUserAdmin(UserAdmin):
    # add fields those needs to be visible while adding the data in form.
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "username",
                    "first_name",
                    "last_name",
                    "email",
                    "password",
                    "is_superuser",
                    "is_active",
                    "is_staff",
                )
            },
        ),
    )


admin.site.register(User, CustomUserAdmin)
