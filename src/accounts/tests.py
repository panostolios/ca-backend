from django.contrib.auth import get_user_model
from django.forms.models import model_to_dict
from django.test import TestCase

from accounts.factories import UserFactory
from accounts.serializers import UserSerializer


class TestUserManager(TestCase):
    def test_create_user(self):
        other_fields = {
            "first_name": "Albert",
            "last_name": "Einstein",
            "email": "a.einstein@genius.com",
        }
        User = get_user_model()
        user = User.objects.create_user(
            username="a.einstein", password="emc2relativity", **other_fields
        )
        self.assertEqual(user.username, "a.einstein")
        self.assertEqual(user.first_name, other_fields["first_name"])
        self.assertEqual(user.last_name, other_fields["last_name"])
        self.assertEqual(user.email, other_fields["email"])
        self.assertTrue(user.is_active)
        self.assertTrue(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_invalid_user(self):
        User = get_user_model()
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(ValueError):
            User.objects.create_user(username="")
        with self.assertRaises(ValueError):
            User.objects.create_user(username=None, password="")

    def test_create_superuser(self):
        other_fields = {
            "first_name": "Albert",
            "last_name": "Einstein",
            "email": "a.einstein@genius.com",
            "is_superuser": False,
            "is_staff": False,
        }
        User = get_user_model()
        # the create_superuser always creates a user with
        # is_superuser, is_active & is_staff set to True
        # no matter what the kwargs say
        user = User.objects.create_superuser(
            "a.superman", "emc2relativity", **other_fields
        )
        self.assertEqual(user.username, "a.superman")
        self.assertEqual(user.first_name, other_fields["first_name"])
        self.assertEqual(user.last_name, other_fields["last_name"])
        self.assertEqual(user.email, other_fields["email"])
        self.assertTrue(user.is_active)
        self.assertTrue(user.is_staff)
        self.assertTrue(user.is_superuser)

    def test_create_invalid_superuser(self):
        User = get_user_model()
        with self.assertRaises(TypeError):
            User.objects.create_superuser()
        with self.assertRaises(TypeError):
            User.objects.create_superuser(username="")
        with self.assertRaises(ValueError):
            User.objects.create_superuser(username=None, password="")


class TestUserSerializer(TestCase):
    def test_user_serializer(self):
        user = UserFactory()
        user_dict = model_to_dict(user)
        serializer = UserSerializer(user)
        self.assertDictEqual(user_dict, serializer.data)
