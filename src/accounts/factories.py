from django.contrib.auth import get_user_model

import factory


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    class Params:
        name = factory.Faker("name", locale="el_GR")
        profile = factory.Faker("profile")
        date_between = factory.Faker("date_time_between", start_date="-178d")

    username = factory.LazyAttribute(lambda obj: obj.profile["username"])
    first_name = factory.LazyAttribute(lambda obj: obj.name.split()[0])
    last_name = factory.LazyAttribute(lambda obj: obj.name.split()[1])
    email = factory.Faker("email")
    is_active = factory.Faker("boolean")
    is_staff = factory.Faker("boolean")
    is_superuser = factory.Faker("boolean")
    password = factory.Faker("password", length=20)
    last_login = factory.LazyAttribute(
        lambda obj: obj.date_between.strftime("%Y-%m-%d %H:%M:%S+02:00")
    )
