import re

from django.forms.models import model_to_dict
from django.utils import timezone
from dateutil import parser


def now():
    return timezone.now().replace(second=0, microsecond=0)


def clean_phone(raw_phone: str) -> str:
    # Remove all non-numeric characters
    # NOTE: see: https://www.howtocallabroad.com/qa/plus-sign.html
    return re.sub(r"[^0-9]", "", raw_phone)


def clean_postal(raw_postal: str) -> str:
    # Remove 'TK' and whitespace
    # Contains greek characters 'ΤΚ'!!!
    return re.sub(r"[ΤΚ\s]", "", raw_postal)


def data_from_order(order) -> dict:
    order_data: dict = model_to_dict(order)
    order_data.pop("id")
    order_data["placement_datetime"] = (
        order_data["placement_datetime"].isoformat().replace("+00:00", "Z")
    )
    order_data["delivery_date"] = (
        order_data["delivery_date"].isoformat().replace("+00:00", "Z")
    )
    order_items: list = []
    for order_item in order.order_items.all():
        orderitem: dict = model_to_dict(order_item)
        orderitem.pop("id")
        orderitem.pop("order")
        orderitem["material"] = {"id": orderitem.pop("material")}
        panel_groups: list = []
        for panel_group in order_item.panel_groups.all():
            panelgroup: dict = model_to_dict(panel_group)
            panelgroup.pop("id")
            panelgroup.pop("order_item")
            panel_groups.append(panelgroup)
        orderitem["panel_groups"] = panel_groups
        order_items.append(orderitem)
    order_data["order_items"] = order_items
    return order_data


def data_from_orders(orders: list) -> list:
    return [data_from_order(order) for order in orders]
