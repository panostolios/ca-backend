from decimal import Decimal
from django.core.exceptions import ValidationError
from django.db import models
from django.dispatch import receiver


from api.validations.order_model_validations import (
    order_after_update,
    order_before_create,
    order_before_update,
)
from utils import now


class CustomerManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .annotate(
                fullname=models.functions.Concat(
                    "first_name", models.Value(" "), "last_name"
                )
            )
        )


class Customer(models.Model):
    class Meta:
        app_label = "api"

    # first_name is a required field (blank=False)
    first_name = models.CharField(max_length=50, blank=False, null=False)
    last_name = models.CharField(max_length=50, blank=True, null=False, default="")
    tax_office = models.CharField(max_length=20, blank=True, default="")
    # Tax Identification Number
    tin = models.CharField(max_length=20, blank=True, default="")
    phone = models.CharField(max_length=20, blank=True, default="")
    email = models.EmailField(max_length=100, blank=True, default="")
    street_address = models.CharField(max_length=100, blank=True, default="")
    postal_code = models.CharField(max_length=20, blank=True, default="")
    city = models.CharField(max_length=50, blank=True, default="")
    country = models.CharField(max_length=50, blank=True, default="")
    notes = models.CharField(max_length=200, blank=True, default="")

    objects = CustomerManager()

    @property
    def full_name(self):
        fullname = f"{self.last_name} {self.first_name}"
        return fullname.strip()

    def __str__(self):
        return self.full_name


class Product(models.Model):
    class Meta:
        app_label = "api"

    class ProductCategory(models.TextChoices):
        GOOD = "good", "Good"
        SERVICE = "service", "Service"

    class ProductType(models.TextChoices):
        INDUSTRIAL_TIMBER = "industrial_timber", "Industrial Timber"
        STRIP = "strip", "Strip"
        WOOD_CUTTING = "wood_cutting", "Wood Cutting"
        HOLE_DRILLING = "hole_drilling", "Hole Drilling"
        STRIP_GLUING = "strip_gluing", "Strip Gluing"

    class ProductUnitOfMeasure(models.TextChoices):
        MM = "mm", "Millimeters"
        M = "m", "Meters"
        SQM = "sq.m", "Square Meters"
        PCS = "pcs", "Pieces"

    name = models.CharField(max_length=100, blank=False, null=False)
    category = models.CharField(max_length=20, choices=ProductCategory.choices)
    type = models.CharField(max_length=50, choices=ProductType.choices)
    unit_of_measure = models.CharField(
        max_length=50,
        choices=ProductUnitOfMeasure.choices,
        blank=True,
        null=True,
        default="",
    )

    def __str__(self) -> str:
        return self.name


class OrderStatus:
    DRAFT = 1
    ACTIVE = 2
    APPROVED = 3
    IN_PROGRESS = 4
    COMPLETED = 5


class BaseOrder(models.Model):
    class Meta:
        abstract = True

    customer = models.ForeignKey(
        Customer, on_delete=models.CASCADE, related_name="orders"
    )
    placement_datetime = models.DateTimeField(blank=True, default=now)
    delivery_date = models.DateField(blank=True, null=True)


class Order(BaseOrder):
    class Meta:
        app_label = "api"

    STATUS_CHOICES = (
        (OrderStatus.DRAFT, "Draft"),
        (OrderStatus.ACTIVE, "New"),
        (OrderStatus.APPROVED, "Approved"),
        (OrderStatus.IN_PROGRESS, "In Progress"),
        (OrderStatus.COMPLETED, "Completed"),
    )

    status = models.IntegerField(
        choices=STATUS_CHOICES, null=False, blank=False, default=0
    )

    @property
    def code(self):
        return f"OR-{self.id:07d}"

    def __str__(self):
        return f"Order #{self.pk}"

    def save(self, *args, **kwargs) -> None:
        old_instance = Order.objects.get(pk=self.pk) if self.pk else None

        (
            order_before_create(self)
            if self.pk is None
            else order_before_update(old_instance, self)
        )

        saved_instance = super().save(*args, **kwargs)

        order_after_update(old_instance, self) if old_instance is not None else None

        return saved_instance


class OriginalOrder(BaseOrder):
    class Meta:
        app_label = "api"

    order = models.OneToOneField("Order", on_delete=models.CASCADE)
    customer = models.ForeignKey(
        Customer, on_delete=models.CASCADE, related_name="original_orders"
    )

    def __str__(self):
        return f"Original of Order #{self.order.pk}"


class BaseOrderItem(models.Model):
    class Meta:
        abstract = True

    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, related_name="order_items"
    )
    material = models.ForeignKey(
        Product,
        null=True,
        on_delete=models.SET_NULL,
        limit_choices_to={"type": "industrial_timber"},
    )

    def __str__(self):
        return f"{self.material.name}"


class OrderItem(BaseOrderItem):
    class Meta:
        app_label = "api"


class OriginalOrderItem(BaseOrderItem):
    class Meta:
        app_label = "api"

    order = models.ForeignKey(
        OriginalOrder, on_delete=models.CASCADE, related_name="original_order_items"
    )
    origin_order_item = models.OneToOneField(
        "OrderItem", on_delete=models.CASCADE, null=True, blank=True
    )


class BasePanelGroup(models.Model):
    class Meta:
        abstract = True

    order_item = models.ForeignKey(
        OrderItem, on_delete=models.CASCADE, related_name="panel_groups"
    )
    quantity = models.PositiveSmallIntegerField()
    # The dimensions should be in millimeters
    x_dimension = models.DecimalField(decimal_places=1, max_digits=5)
    y_dimension = models.DecimalField(decimal_places=1, max_digits=5)

    @property
    def total_area(self):
        # Returns the area from the two dimensions in square meters
        return Decimal(self.quantity * self.x_dimension * self.y_dimension / 1000000)

    @property
    def material(self):
        return self.order_item.material

    @property
    def ratio_x_y(self):
        return self.x_dimension / self.y_dimension


class PanelGroup(BasePanelGroup):
    class Meta:
        app_label = "api"

    def __str__(self):
        return f"PanelGroup of '{self.order_item.material.name}' material"


class OriginalPanelGroup(BasePanelGroup):
    class Meta:
        app_label = "api"

    order_item = models.ForeignKey(
        OriginalOrderItem,
        on_delete=models.CASCADE,
        related_name="original_panel_groups",
    )

    origin_panel_group = models.OneToOneField(
        "PanelGroup", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return f"Original of PanelGroup of '{self.order_item.material.name}' material"


class BaseStripPosition(models.Model):
    class Meta:
        abstract = True

    class Position(models.TextChoices):
        XTOP = "x_top", "X Top"
        XBOTTOM = "x_bottom", "X Bottom"
        YTOP = "y_left", "Y Left"
        YBOTTOM = "y_right", "Y Right"

    panel_group = models.ForeignKey(
        PanelGroup, on_delete=models.CASCADE, related_name="strip_positions"
    )
    strip = models.ForeignKey(
        Product,
        on_delete=models.SET_NULL,
        limit_choices_to={"type": "strip"},
        null=True,
    )
    strip_position = models.CharField(max_length=20, choices=Position.choices)


class StripPosition(BaseStripPosition):
    class Meta:
        app_label = "api"


class OriginalStripPosition(BaseStripPosition):
    class Meta:
        app_label = "api"

    panel_group = models.ForeignKey(
        OriginalPanelGroup,
        on_delete=models.CASCADE,
        related_name="original_strip_positions",
    )


@receiver(models.signals.pre_save, sender=StripPosition)
def pre_save_StripPosition(sender, instance, **kwargs):
    sps = StripPosition.objects.filter(panel_group=instance.panel_group).exclude(
        pk=instance.pk
    )
    for sp in sps:
        if sp.strip_position == instance.strip_position:
            raise ValidationError(
                f"There is already a strip on this position {instance.strip_position} [{instance.panel_group.order_item.order.id}]"
            )
