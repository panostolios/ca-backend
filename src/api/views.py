from django_filters import rest_framework as filters
from rest_framework import filters as rf_filters
from django.contrib.auth import login, logout
from django.shortcuts import get_object_or_404
from django_weasyprint.views import WeasyTemplateView
from rest_framework import status
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Customer, Order, OriginalOrder, OrderItem, Product
from api.filters import CustomerFilter, OrderFilter, ProductFilter
from api.serializers import (
    CustomerSerializer,
    FullOrderSerializer,
    OrderSerializer,
    OriginalOrderSerializer,
    OrderItemSerializer,
    LoginSerializer,
    ProductSerializer,
)
from accounts.serializers import UserSerializer


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    filter_backends = (filters.DjangoFilterBackend, rf_filters.OrderingFilter)
    filterset_class = CustomerFilter
    permission_classes = (permissions.IsAuthenticated,)
    ordering = ["last_name"]

    @action(methods=["get"], detail=False)
    def count(self, request, pk=None):
        customer_count = Customer.objects.count()
        return Response(customer_count)


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ProductFilter
    permission_classes = (permissions.IsAuthenticated,)

    @action(methods=["get"], detail=False)
    def count(self, request, pk=None):
        product_count = Product.objects.count()
        return Response(product_count)


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = FullOrderSerializer
    # Don't use PATCH method
    http_method_names = ["get", "post", "head", "put", "delete"]
    filter_backends = (filters.DjangoFilterBackend, rf_filters.OrderingFilter)
    filterset_class = OrderFilter
    permission_classes = (permissions.IsAuthenticated,)
    ordering = ["-placement_datetime"]

    def get_serializer_class(self):
        # For lists use the OrderSerializer
        if self.action == "list":
            return OrderSerializer
        # For the rest use the default
        return super().get_serializer_class()

    @action(methods=["get"], detail=False)
    def count(self, request, pk=None):
        order_count = Order.objects.count()
        return Response(order_count)


class OrderItemViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer


class OriginalOrderViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = OriginalOrderSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        original_order = get_object_or_404(OriginalOrder, order__pk=pk)
        serializer = OriginalOrderSerializer(original_order)
        return Response(serializer.data)


class ListOrderStatusesView(APIView):
    """
    View to list all order statuses in the system.

    """

    http_method_names = ["get"]

    def get(self, request, format=None):
        """
        Return a list of all order statuses.
        """
        statuses: [dict] = []
        for status in Order.STATUS_CHOICES:
            statuses.append({"id": status[0], "text": status[1]})
        return Response(statuses)


class CustomerCountView(APIView):
    """
    Return the number of Customers

    """

    http_method_names = ["get"]

    def get(self, request, format=None):
        customer_count = Customer.objects.count()
        return Response(customer_count)


class LoginView(APIView):
    # This view should be accessible also for unauthenticated users.
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = LoginSerializer(
            data=self.request.data, context={"request": self.request}
        )
        serializer.is_valid(raise_exception=True)
        user: User = serializer.validated_data["user"]
        responseUser = UserSerializer(user)
        login(request, user)
        print(request.META.get("CSRF_COOKIE"))
        # prepare the response
        responseDict = {
            "csrf_token": request.META.get("CSRF_COOKIE"),
            "session_id": self.request.session.session_key,
            "user": responseUser.data,
        }
        response = Response(responseDict)
        response.status_code = status.HTTP_202_ACCEPTED
        print(responseDict["session_id"])
        return response


class LogoutView(APIView):

    def get(self, request, format=None):
        logout(request)
        return Response({"success": "User logged out"}, status=status.HTTP_200_OK)


class PDFView(WeasyTemplateView):
    pdf_filename = "output.pdf"
    pdf_attachment = False
    template_name = "api/order/order.html"
    pdf_stylesheets = [
        "api/static/api/css/tw-style.css",
        "api/static/api/css/style.css",
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        order = Order.objects.get(pk=self.kwargs["id"])
        context["order"] = order
        context["customer"] = order.customer
        return context
