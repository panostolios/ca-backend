from django.test import TestCase
import factory

from api.models import Customer
from api.factories import CustomerFactory


with factory.Faker.override_default_locale("el_GR"):
    customer1 = factory.build(
        dict,
        FACTORY_CLASS=CustomerFactory,
        first_name="Albert",
        last_name="Einstein",
        tax_office="A Athenon",
        tin="658347895",
        phone="6549879",
        email="albert@einstein.com",
    )


class TestCustomerModel(TestCase):
    def setUp(self):
        self.customer1 = Customer(**customer1)

    def test_customer_count(self):
        count_before_save = Customer.objects.count()
        self.customer1.save()
        count_after_save = Customer.objects.count()
        self.assertNotEqual(count_before_save, count_after_save)
        self.assertEqual(count_after_save - count_before_save, 1)

    def test_customer_filter_fullname_exact(self):
        first_name: str = "Marie"
        last_name: str = "Curie"
        with factory.Faker.override_default_locale("el_GR"):
            CustomerFactory.create_batch(10)
            CustomerFactory.create_batch(3, first_name=first_name, last_name=last_name)
        curies = Customer.objects.filter(fullname=f"{first_name} {last_name}")
        self.assertEqual(len(curies), 3)
        for marie in curies:
            self.assertEqual(marie.first_name, first_name)
            self.assertEqual(marie.last_name, last_name)

    def test_customer_filter_fullname_case_insensitive(self):
        first_name: str = "Marie"
        last_name: str = "Curie"
        with factory.Faker.override_default_locale("el_GR"):
            # Create 10 random customers
            CustomerFactory.create_batch(10)
            # Create 3 customers with provided full name
            CustomerFactory.create_batch(
                3, first_name=first_name.lower(), last_name=last_name.lower()
            )
        curies = Customer.objects.filter(fullname__iexact=f"{first_name} {last_name}")
        self.assertEqual(len(curies), 3)
        for marie in curies:
            self.assertEqual(marie.first_name.lower(), first_name.lower())
            self.assertEqual(marie.last_name.lower(), last_name.lower())
