import decimal, json
from typing import Any, Dict, List, TYPE_CHECKING

from datetime import datetime, date
import factory
from rest_framework import exceptions
from rest_framework.test import APITestCase

from api.models import (
    Order,
    OriginalOrder,
    OrderItem,
    PanelGroup,
    Product,
    StripPosition,
)
from api.factories import FullOrderFactory
from utils import data_from_orders

from unittest import skip

from api.serializers import (
    FullOrderSerializer,
)

if TYPE_CHECKING:
    from django.db.models.query import QuerySet


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, decimal.Decimal):
            return str(float(obj))
        if isinstance(obj, date) or isinstance(obj, datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)


class TestOrderSerializer(APITestCase):
    def setUp(self) -> None:
        self.maxDiff = None
        with factory.Faker.override_default_locale("el_GR"):
            self.orders: List[Order] = FullOrderFactory.create_batch(5)
            # Let's pick the 3rd order
            self.order: Order = self.orders[2]
        self.order_data: List[Dict[str, Any]] = data_from_orders(self.orders)

    # @skip("Skip test")
    def test_create_order(self):
        test_order: QuerySet = Order.objects.filter(pk=2)
        order = list(test_order.values())[0]
        order.pop("id")
        order["customer"] = order.pop("customer_id")
        order_items = OrderItem.objects.filter(order=test_order[0])
        order_items_list = []
        for order_item in order_items:
            panel_groups = list(
                PanelGroup.objects.filter(order_item=order_item.pk).values()
            )
            for panel_group in panel_groups:
                strip_positions = list(
                    StripPosition.objects.filter(
                        panel_group=panel_group.pop("id")
                    ).values()
                )
                for strip_position in strip_positions:
                    strip_position.pop("id")
                    strip_position.pop("panel_group_id")
                    strip_id = strip_position.pop("strip_id")
                    strip_position["strip"] = {
                        "id": strip_id,
                    }
                panel_group["strip_positions"] = strip_positions
                panel_group.pop("order_item_id")
            order_item_data = {}
            order_item_data["panel_groups"] = panel_groups
            order_item_data["material"] = {
                "id": order_item.material.pk,
            }
            order_items_list.append(order_item_data)

        order["order_items"] = order_items_list

        data = json.loads(json.dumps(order, cls=Encoder))

        serializer = FullOrderSerializer(data=data)
        count_before = Order.objects.count()
        if serializer.is_valid(raise_exception=True):
            created_order = serializer.save()

        count_after = Order.objects.count()

        self.assertEquals(count_after, count_before + 1)

        self.assertEquals(created_order.placement_datetime, order["placement_datetime"])
        self.assertEquals(created_order.customer.id, order["customer"])
        self.assertEquals(
            created_order.order_items.first().material.id,
            order["order_items"][0]["material"]["id"],
        )
        self.assertEquals(
            created_order.order_items.first().panel_groups.first().quantity,
            order["order_items"][0]["panel_groups"][0]["quantity"],
        )
        self.assertEquals(
            created_order.order_items.first()
            .panel_groups.first()
            .strip_positions.first()
            .strip_position,
            order["order_items"][0]["panel_groups"][0]["strip_positions"][0][
                "strip_position"
            ],
        )

        for order_item in created_order.order_items.all():
            for panel_group in order_item.panel_groups.all():
                self.assertGreaterEqual(
                    panel_group.x_dimension,
                    panel_group.y_dimension,
                )

    # @skip("Skip test")
    def test_serialization(self):
        serializer = FullOrderSerializer(self.order)
        data = serializer.data

        self.assertEquals(data["delivery_date"], str(self.order.delivery_date))
        self.assertEquals(data["customer"], self.order.customer.id)
        self.assertEquals(
            data["order_items"][0]["material"]["id"],
            self.order.order_items.first().material.pk,
        )
        self.assertEquals(
            data["order_items"][0]["material"]["name"],
            self.order.order_items.first().material.name,
        )
        self.assertEquals(
            data["order_items"][0]["panel_groups"][0]["x_dimension"],
            str(self.order.order_items.first().panel_groups.first().x_dimension),
        )

    # @skip("Skip test")
    def test_update_order(self):
        order = list(Order.objects.filter(pk=self.order.pk).values())[0]

        order["customer"] = order.pop("customer_id")
        order_items = list(OrderItem.objects.filter(order=self.order).values())
        for order_item in order_items:
            order_item.pop("order_id")

            material = Product.objects.filter(
                pk=order_item.pop("material_id")
            ).values()[0]

            order_item["material"] = material

            panel_groups = list(
                PanelGroup.objects.filter(order_item=order_item["id"]).values()
            )
            for panel_group in panel_groups:
                panel_group.pop("order_item_id")

            order_item["panel_groups"] = panel_groups

        order["order_items"] = order_items

        order["status"] = 4
        order["customer"] = 2
        order["order_items"][-1]["panel_groups"][0]["x_dimension"] = decimal.Decimal(
            "150"
        )
        order["order_items"][-1]["panel_groups"][0]["y_dimension"] = decimal.Decimal(
            "250"
        )

        serializer = FullOrderSerializer(self.order, data=order)
        if serializer.is_valid(raise_exception=True):
            serializer.save()

        self.assertEquals(self.order.status, order["status"])
        self.assertEquals(self.order.customer.id, order["customer"])
        self.assertEquals(
            self.order.order_items.first().id, order["order_items"][0]["id"]
        )
        self.assertEquals(
            self.order.order_items.first().material.pk,
            order["order_items"][0]["material"]["id"],
        )
        self.assertEquals(
            self.order.order_items.first().material.name,
            order["order_items"][0]["material"]["name"],
        )
        self.assertEquals(
            self.order.order_items.last().panel_groups.first().y_dimension,
            order["order_items"][-1]["panel_groups"][0]["y_dimension"],
        )

    # @skip("Skip test")
    def test_prevent_order_to_draft(self):
        order = Order.objects.exclude(status=1).first()

        serializer = FullOrderSerializer(order)
        data = json.loads(json.dumps(serializer.data))
        data["status"] = 1

        serializer = FullOrderSerializer(order, data)
        if serializer.is_valid():
            try:
                serializer.save()
            except exceptions.ValidationError as error:
                self.assertEquals(
                    "You cannot set the Order status back to 'Draft'", error.detail[0]
                )

    # @skip("Skip test")
    def test_create_original_order(self):
        order = Order.objects.filter(status=1).first()
        data = FullOrderSerializer(order).data
        data["status"] = 2
        serializer = FullOrderSerializer(order, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.errors)

        original_order = OriginalOrder.objects.get(order=order)

        self.assertEquals(original_order.order, order)
        self.assertEquals(
            order.order_items.count(), original_order.original_order_items.count()
        )
        self.assertEquals(
            order.order_items.first().panel_groups.count(),
            original_order.original_order_items.first().original_panel_groups.count(),
        )
        self.assertEqual(
            order.order_items.first().panel_groups.first().strip_positions.count(),
            original_order.original_order_items.first()
            .original_panel_groups.first()
            .original_strip_positions.count(),
        )
