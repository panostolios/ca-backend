import re
from django.test import TestCase

import factory

from api.factories import CustomerFactory
from utils import clean_phone, clean_postal
from api.models import Customer


class TestCustomerFactory(TestCase):
    def test_clean_functions(self):
        invalid_phone_chars = re.compile(r"[-()\s]")
        invalid_postal_chars = re.compile(r"[ΤΚ\s]")
        # test with greek locale
        with factory.Faker.override_default_locale("el_GR"):
            customers = factory.build_batch(dict, 10, FACTORY_CLASS=CustomerFactory)
            customers[0]["phone"] = "(555) - 3249"
            for customer in customers:
                phone = clean_phone(customer["phone"])
                phone_contains_invalid_chars = invalid_phone_chars.match(phone)
                self.assertFalse(phone_contains_invalid_chars)

                postal_code = clean_postal(customer["postal_code"])
                postal_contains_invalid_chars = invalid_postal_chars.match(postal_code)
                self.assertFalse(postal_contains_invalid_chars)

    def test_customer_factory(self):
        # test with greek locale
        customer_details: dict = {
            "first_name": "Albert",
            "last_name": "Einstein",
            # assigning 'phone' directly overrides LazyAttribute
            "phone": "+306845856357",
            # assigning 'postal_code' directly overrides LazyAttribute
            "postal_code": "48354",
            "country": "Ελλάδα",
        }
        with factory.Faker.override_default_locale("el_GR"):
            customer1 = factory.build(dict, FACTORY_CLASS=CustomerFactory)
            customer2 = CustomerFactory.build(**customer_details)

        self.assertEqual(type(customer1), dict)

        self.assertEqual(customer2.first_name, customer_details["first_name"])
        self.assertEqual(customer2.last_name, customer_details["last_name"])
        self.assertEqual(customer2.phone, customer_details["phone"])
        self.assertEqual(customer2.postal_code, customer_details["postal_code"])
        self.assertEqual(customer2.country, customer_details["country"])
