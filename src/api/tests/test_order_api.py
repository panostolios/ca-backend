import json
import random
from datetime import date
from decimal import Decimal
from typing import List, TYPE_CHECKING

from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse
import factory
from rest_framework import status
from rest_framework.test import APITestCase

from api.factories import FullOrderFactory
from api.models import Order, OrderStatus, OrderItem
from api.serializers import OrderSerializer, FullOrderSerializer, OrderItemSerializer
from utils import data_from_orders

from unittest import skipIf

if TYPE_CHECKING:
    from django.db.models.query import QuerySet

User = get_user_model()
credentials = {"username": "whatever", "password": "secret"}


class TestOrderListAPI(APITestCase):
    def setUp(self):
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        self.maxDiff = None
        with factory.Faker.override_default_locale("el_GR"):
            self.orders: list = FullOrderFactory.create_batch(3)

    # @skipIf(True, "Skip test")
    def test_get_order_list(self):
        # To get a list of orders use FullOrderSerializer
        response = self.client.get(reverse("order-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_data: list = response.json()
        serializer = OrderSerializer(self.orders, many=True)
        self.assertEqual(len(response_data), len(serializer.data))


class TestOrderFilters(APITestCase):
    def setUp(self):
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        self.maxDiff = None
        with factory.Faker.override_default_locale("el_GR"):
            self.orders: list = FullOrderFactory.create_batch(3)

    # @skipIf(True, "Skip test")
    def test_filter_orderlist_by_valid_status(self):
        status_value: int = 2
        orders_with_specific_status: QuerySet = Order.objects.filter(
            status=status_value
        )
        count: int = orders_with_specific_status.count()
        response = self.client.get(reverse("order-list"), {"status": status_value})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_data: list = response.json()
        self.assertEqual((len(response_data)), count)
        try:
            self.assertEqual(response_data[0]["status"], status_value)
        except IndexError:
            self.assertEqual(count, 0)

    # @skipIf(True, "Skip test")
    def test_filter_orderlist_by_invalid_status(self):
        invalid_status: int = 20
        response = self.client.get(reverse("order-list"), {"status": invalid_status})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # @skipIf(True, "Skip test")
    def test_filter_delivery_date(self):
        with factory.Faker.override_default_locale("el_GR"):
            # add some more orders
            extra_orders: list = FullOrderFactory.create_batch(20)
        delivery_dates: List[date] = []
        for order in self.orders + extra_orders:
            delivery_dates.append(order.delivery_date)
        # create a list to test
        reference_date: date = delivery_dates[10]
        test_delivery_dates: List[date] = [
            d_date for d_date in delivery_dates if d_date >= reference_date
        ]
        no_of_all_dates: int = len(delivery_dates)
        no_of_test_dates: int = len(test_delivery_dates)

        # test random date with no matches
        response = self.client.get(
            reverse("order-list"), {"delivery_date": "1990-01-01"}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), 0)
        else:
            print("Please provide a valid delivery date for test_filter_delivery_date")

        # test exact match
        response = self.client.get(
            reverse("order-list"), {"delivery_date": reference_date}
        )

        for data in response.data:
            self.assertEqual(str(reference_date), data["delivery_date"])

        # test gte
        response = self.client.get(
            reverse("order-list"), {"delivery_date__gte": reference_date}
        )
        self.assertEqual(len(response.data), no_of_test_dates)

        # test lt
        response = self.client.get(
            reverse("order-list"), {"delivery_date__lt": reference_date}
        )
        self.assertEqual(len(response.data), no_of_all_dates - no_of_test_dates)

    # @skipIf(True, "Skip test")
    def test_filter_placement_datetime(self):
        with factory.Faker.override_default_locale("el_GR"):
            # add some more orders
            extra_orders: list = FullOrderFactory.create_batch(20)
        placement_datetimes: List[date] = []
        for order in self.orders + extra_orders:
            placement_datetimes.append(order.placement_datetime)
        # create a list to test
        reference_datetime: date = placement_datetimes[10]
        test_placement_datetimes: List[date] = [
            d_date for d_date in placement_datetimes if d_date >= reference_datetime
        ]
        no_of_all_dates: int = len(placement_datetimes)
        no_of_test_dates: int = len(test_placement_datetimes)

        # test random date with no matches
        response = self.client.get(
            reverse("order-list"), {"placement_datetime": "2019-09-12T19:34:22"}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), 0)
        else:
            print(
                "Please provide a valid delivery date for test_filter_placement_datetime"
            )

        # test exact match
        response = self.client.get(
            reverse("order-list"), {"placement_datetime": reference_datetime}
        )
        for data in response.data:
            self.assertEqual(
                str(reference_datetime.isoformat()).replace("+00:00", "Z"),
                data["placement_datetime"],
            )

        # test gte
        response = self.client.get(
            reverse("order-list"), {"placement_datetime__gte": reference_datetime}
        )
        self.assertEqual(len(response.data), no_of_test_dates)

        # test lt
        response = self.client.get(
            reverse("order-list"), {"placement_datetime__lt": reference_datetime}
        )
        self.assertEqual(len(response.data), no_of_all_dates - no_of_test_dates)


class TestSingleOrderAPI(APITestCase):
    def setUp(self):
        user = User.objects.create_user(**credentials)
        self.client.force_login(user=user)
        with factory.Faker.override_default_locale("el_GR"):
            self.orders: list = FullOrderFactory.create_batch(3)
        self.orders_data: list = data_from_orders(self.orders)
        self.pks: list = Order.objects.values_list("id", flat=True)

    # @skipIf(True, "Skip test")
    def test_get_single_order(self):
        pk: int = random.choice(self.pks)
        response = self.client.get(reverse("order-detail", kwargs={"pk": pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        order = Order.objects.get(pk=pk)
        serializer = FullOrderSerializer(order)
        self.assertEqual(response.data, serializer.data)

    # @skipIf(True, "Skip test")
    def test_post_valid_single_order(self):
        # pick a random element from the list
        random_index: int = random.choice(range(len(self.orders)))
        order: dict = self.orders_data[random_index]
        ords = list(Order.objects.all().values())
        ord = ords[random_index]
        count_before: int = Order.objects.count()
        response = self.client.post(reverse("order-list"), order, format="json")
        count_after: int = Order.objects.count()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(count_before + 1, count_after)

    # @skipIf(True, "Skip test")
    def test_post_invalid_single_order(self):
        # Create invalid data
        invalid_pk: int = 1000
        invalid_status: int = 100

        # pick a random element from the list
        random_index: int = random.choice(range(len(self.orders)))
        order: dict = self.orders_data[random_index]
        order["customer"] = invalid_pk
        order["status"] = invalid_status
        order["delivery_date"] = "2018-10-01"

        response = self.client.post(reverse("order-list"), order, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        invalid_pk_error_message: str = (
            f'Λάθος κλειδί "{invalid_pk}" - το αντικείμενο δεν υπάρχει.'
            if settings.LANGUAGE_CODE == "el-gr"
            else f'Invalid pk "{invalid_pk}" - object does not exist.'
        )
        self.assertEqual(
            response.data["customer"][0],
            invalid_pk_error_message,
        )
        invalid_status_error_message: str = (
            f'Το "{invalid_status}" δεν είναι έγκυρη επιλογή.'
            if settings.LANGUAGE_CODE == "el-gr"
            else f'"{invalid_status}" is not a valid choice.'
        )
        self.assertEqual(response.data["status"][0], invalid_status_error_message)

    # @skipIf(True, "Skip test")
    def test_patch_single_order(self):
        pk: int = 1
        response = self.client.patch(
            reverse("order-detail", kwargs={"pk": pk}), {"customer": 1}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    # @skipIf(True, "Skip test")
    def test_put_single_order(self):
        order: dict = self.orders_data[0]
        order["customer"] = 2
        order["status"] = 3
        order["delivery_date"] = "2022-10-01"
        for orderitem_id, orderitem in enumerate(order["order_items"]):
            orderitem["id"] = orderitem_id + 1
        panel_group = order["order_items"][0]["panel_groups"][0]
        panel_group["quantity"] = 3000
        panel_group["x_dimension"] = Decimal(123.4)
        if Decimal(panel_group["x_dimension"]) < Decimal(panel_group["y_dimension"]):
            panel_group["x_dimension"], panel_group["y_dimension"] = (
                panel_group["y_dimension"],
                panel_group["x_dimension"],
            )

        pk: int = 1
        order["id"] = pk
        response = self.client.put(
            reverse("order-detail", kwargs={"pk": pk}), order, format="json"
        )
        response_json: dict = response.json()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_json["customer"], order["customer"])
        self.assertEqual(response_json["status"], order["status"])
        self.assertEqual(
            response_json["order_items"][0]["panel_groups"][0]["quantity"],
            order["order_items"][0]["panel_groups"][0]["quantity"],
        )
        self.assertEqual(
            Decimal(response_json["order_items"][0]["panel_groups"][0]["x_dimension"]),
            order["order_items"][0]["panel_groups"][0]["x_dimension"],
        )
        self.assertEqual(response_json["delivery_date"], order["delivery_date"])

    # @skipIf(True, "Skip test")
    def test_delete_single_order(self):
        pk: int = 1

        response = self.client.delete(reverse("order-detail", kwargs={"pk": pk}))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class TestOriginalOrderAPI(APITestCase):
    def setUp(self):
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        with factory.Faker.override_default_locale("el_GR"):
            self.orders: list = FullOrderFactory.create_batch(3)

    # @skipIf(True, "Skip test")
    def test_get_original_orders(self):
        order = Order.objects.filter(status=OrderStatus.DRAFT).first()
        order.status = OrderStatus.ACTIVE
        order.save()
        response = self.client.get(reverse("original-order", kwargs={"pk": order.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["order"], order.pk)
        self.assertEqual(
            order.order_items.count(), len(response.data["original_order_items"])
        )


class TestOrderStatusesAPI(APITestCase):
    def setUp(self):
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        with factory.Faker.override_default_locale("el_GR"):
            self.orders: list = FullOrderFactory.create_batch(3)

    # @skipIf(True, "Skip test")
    def test_get_all_order_statuses(self):
        response = self.client.get(reverse("order-statuses"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        order_statuses_from_model = Order.STATUS_CHOICES
        for item in zip(order_statuses_from_model, response.data):
            self.assertEqual(item[0][0], item[1]["id"])
            self.assertEqual(item[0][1], item[1]["text"])


class TestOrderItemsAPI(APITestCase):
    def setUp(self):
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        with factory.Faker.override_default_locale("el_GR"):
            self.orders: list = FullOrderFactory.create_batch(3)

    # @skipIf(True, "Skip test")
    def test_get_all_orderitems(self):
        response = self.client.get(reverse("orderitem-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), OrderItem.objects.count())

    # @skipIf(True, "Skip test")
    def test_get_single_orderitem(self):
        orderitem = OrderItem.objects.first()
        response = self.client.get(
            reverse("orderitem-detail", kwargs={"pk": orderitem.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], orderitem.pk)

    # @skipIf(True, "Skip test")
    def test_create_orderitem(self):
        order = Order.objects.first()
        orderitem = OrderItem.objects.first()

        serialized_data = OrderItemSerializer(orderitem).data
        del serialized_data["id"]

        response = self.client.post(
            reverse("orderitem-list"), serialized_data, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["order"], orderitem.order.pk)
