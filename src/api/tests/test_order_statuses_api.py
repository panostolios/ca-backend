from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status

from api.models import Order


class TestOrderStatusesAPI(APITestCase):
    def test_get_all_order_statuses(self):
        response = self.client.get(reverse("order-statuses"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        order_statuses_from_model = Order.STATUS_CHOICES
        for item in zip(order_statuses_from_model, response.data):
            self.assertEqual(item[0][0], item[1]["id"])
            self.assertEqual(item[0][1], item[1]["text"])
