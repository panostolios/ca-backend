import datetime
from django.test import TestCase

import factory

from api.models import Customer, Order, OrderItem
from api.factories import (
    CustomerFactory,
    FullOrderFactory,
    OrderFactory,
    OrderItemFactory,
    OrderStatus,
)


class TestOrderItemFactory(TestCase):
    def test_random_orderitem_factory_as_dict(self):
        with factory.Faker.override_default_locale("el_GR"):
            orderitem: dict = factory.build(dict, FACTORY_CLASS=OrderItemFactory)
        self.assertTrue(isinstance(orderitem, dict))
        # Remove 'mm' from string
        material_description: str = orderitem["material"].replace("mm", "")
        material_choices: list = OrderItemFactory._meta.parameters[
            "material_choices"
        ].value

        words: list = material_description.split()
        common_material_words: list = [i for i in material_choices if i in words]
        # Check if material is composed from the right keywords
        self.assertTrue(common_material_words)

        color_choices: list = OrderItemFactory._meta.parameters["color_choices"].value
        common_color_words: list = [i for i in color_choices if i in words]
        # Check if material color is composed from the right keywords
        self.assertTrue(common_color_words)

        thickness_choices: list = OrderItemFactory._meta.parameters[
            "thickness_choices"
        ].value
        # Convert numbers to strings for comparison
        thickness_choices: list = list(map(str, thickness_choices))
        common_thickness_values: list = [i for i in thickness_choices if i in words]
        # Check if material thickness is composed from the values
        self.assertTrue(common_thickness_values)

    def test_random_orderitem_factory_as_object(self):
        with factory.Faker.override_default_locale("el_GR"):
            orderitem: dict = OrderItemFactory.create()
        self.assertTrue(isinstance(orderitem, OrderItem))
        self.assertTrue(isinstance(orderitem.order, Order))
        self.assertTrue(isinstance(orderitem.order.customer, Customer))

    def test_specific_orderitem_factory(self):
        orderitem_details: dict = {
            "material": "Rosewood Melamine 25mm",
            "quantity": 20,
            "x_dimension": 125.1,
            "y_dimension": 654.1,
        }

        with factory.Faker.override_default_locale("el_GR"):
            orderitem: OrderItem = OrderItemFactory.build(**orderitem_details)
        self.assertEqual(orderitem.material, orderitem_details["material"])
        self.assertEqual(orderitem.quantity, orderitem_details["quantity"])
        self.assertEqual(orderitem.x_dimension, orderitem_details["x_dimension"])
        self.assertEqual(orderitem.y_dimension, orderitem_details["y_dimension"])


class TestOrderFactory(TestCase):
    def test_specific_order_factory(self):
        with factory.Faker.override_default_locale("el_GR"):
            customer = CustomerFactory.create()
            order = OrderFactory.create(customer=customer)
        self.assertEqual(order.customer, customer)
        self.assertEqual(type(order.placement_datetime), datetime.datetime)
        self.assertEqual(type(order.delivery_date), datetime.date)
        self.assertIn(order.status, vars(OrderStatus).values())

    def test_random_order_factory_as_dict(self):
        with factory.Faker.override_default_locale("el_GR"):
            order: dict = factory.build(dict, FACTORY_CLASS=OrderFactory)
        self.assertTrue(isinstance(order, dict))

    def test_full_order(self):
        with factory.Faker.override_default_locale("el_GR"):
            orderitem = OrderItemFactory(order__customer__first_name="Joe")
        self.assertEqual(orderitem.order.customer.first_name, "Joe")


class TestFullOrderFactory(TestCase):
    def test_full_order_factory(self):
        with factory.Faker.override_default_locale("el_GR"):
            order = FullOrderFactory()
        for orderitem in order.order_items.all():
            self.assertEqual(orderitem.order.id, order.id)
