from decimal import Decimal
from typing import Dict
from django.core.exceptions import ValidationError
from django.test import TestCase
import factory


from api.models import OrderItem, PanelGroup, StripPosition
from api.factories import (
    CustomerFactory,
    OrderFactory,
    OrderItemFactory,
    ProductFactory,
    PanelGroupFactory,
)

panel_group1: Dict = {
    "quantity": 14,
    "x_dimension": 255.5,
    "y_dimension": 325,
}
panel_group2: Dict = {
    "quantity": 25,
    "x_dimension": 150,
    "y_dimension": 100,
}
panel_group3: Dict = {
    "quantity": 5,
    "x_dimension": 175,
    "y_dimension": 225,
}


class TestOrderItemModel(TestCase):
    def setUp(self):
        with factory.Faker.override_default_locale("el_GR"):
            self.customer = CustomerFactory()
            self.order = OrderFactory(customer=self.customer)
            self.material1 = ProductFactory(industrial_timber=True)
            self.material2 = ProductFactory(industrial_timber=True)
            self.strip1 = ProductFactory(strip=True)
            self.strip2 = ProductFactory(strip=True)

            self.order_item1 = OrderItemFactory.build(
                material=self.material1, order=self.order
            )
            self.order_item2 = OrderItemFactory(
                material=self.material2, order=self.order
            )
            self.panel_group1: PanelGroup = PanelGroupFactory.build(
                **panel_group1, order_item=self.order_item1
            )
            self.panel_group2: PanelGroup = PanelGroupFactory(
                **panel_group2, order_item=self.order_item2
            )
            self.panel_group3: PanelGroup = PanelGroupFactory(
                **panel_group3, order_item=self.order_item2
            )
            try:
                StripPosition(
                    panel_group=self.panel_group2,
                    strip=self.strip1,
                    strip_position=StripPosition.Position.XTOP,
                ).save()
            except ValidationError:
                pass

            try:
                StripPosition(
                    panel_group=self.panel_group3,
                    strip=self.strip2,
                    strip_position=StripPosition.Position.YTOP,
                ).save()
            except ValidationError:
                pass

    def test_instance_is_saved(self):
        count_before = OrderItem.objects.count()
        self.order_item1.save()
        count_after = OrderItem.objects.count()
        self.assertNotEqual(count_before, count_after)
        self.assertEqual(count_after - count_before, 1)

    def test_fields(self):
        fields = [field.name for field in self.panel_group2._meta.get_fields()]
        self.assertIn("quantity", fields)
        self.assertIn("x_dimension", fields)
        self.assertIn("y_dimension", fields)

    def test_area_calculation(self):
        # The values provided in x_dimension & y_dimension are in mm.
        # The returned total area of each OrderItem is in sq.meters.
        self.order_item1.save()
        self.panel_group1.save()
        # convert area1 to sq.mm
        area1: Decimal = self.panel_group1.total_area * 1000000
        calculated_area1: Decimal = Decimal(1)
        for key, value in panel_group1.items():
            if key in ("quantity", "x_dimension", "y_dimension"):
                calculated_area1 = calculated_area1 * Decimal(value)
        # convert area2 to sq.mm
        area2: Decimal = self.panel_group2.total_area * 1000000
        calculated_area2: Decimal = Decimal(1)
        for key, value in panel_group2.items():
            if key in ("quantity", "x_dimension", "y_dimension"):
                calculated_area2 = calculated_area2 * Decimal(value)
        self.assertAlmostEqual(area1, calculated_area1, 3)
        self.assertAlmostEqual(area2, calculated_area2)

    def test_str(self):
        order_item_str: str = f"{self.order_item1.material.name}"
        self.assertEqual(str(self.order_item1), order_item_str)

    def test_strip_position(self):
        with self.assertRaises(
            ValidationError, msg="There is already a strip on this position X Top"
        ):
            StripPosition(
                panel_group=self.panel_group2,
                strip=self.strip2,
                strip_position=StripPosition.Position.XTOP,
            ).save()
        with self.assertRaises(
            ValidationError, msg="There is already a strip on this position Y Top"
        ):
            StripPosition(
                panel_group=self.panel_group3,
                strip=self.strip2,
                strip_position=StripPosition.Position.YTOP,
            ).save()
