from unittest import skip
from django.core.exceptions import ValidationError
from django.test import TestCase
import factory

from api.models import Customer, Order, OrderItem, OrderStatus, StripPosition
from api.factories import (
    CustomerFactory,
    FullOrderFactory,
    OrderItemFactory,
    PanelGroupFactory,
)
from api.management.commands.seed_db import Command

with factory.Faker.override_default_locale("el_GR"):
    customer = factory.build(
        dict,
        FACTORY_CLASS=CustomerFactory,
        first_name="Artemis",
        last_name="Agleouras",
    )

order = {"status": OrderStatus.ACTIVE}

panel_groups = [
    {
        "quantity": 14,
        "x_dimension": 255.5,
        "y_dimension": 325,
    },
    {
        "quantity": 25,
        "x_dimension": 150,
        "y_dimension": 100,
    },
    {
        "quantity": 32,
        "x_dimension": 852.5,
        "y_dimension": 749.3,
    },
]


class TestOrderModel(TestCase):
    def setUp(self):
        cmd = Command()
        cmd.handle(verbosity=0, customers_to_create=1)

    # @skip("Skip test")
    def test_instance_is_saved(self):
        customer = Customer.objects.first()
        order1 = Order(status=OrderStatus.DRAFT, customer=customer)
        count_before = Order.objects.count()
        order1.save()
        count_after = Order.objects.count()
        self.assertNotEqual(count_before, count_after)
        self.assertEqual(count_after - count_before, 1)

    # @skip("Skip test")
    def test_str(self):
        order = Order.objects.first()
        self.assertEqual(str(order), f"Order #{order.id}")

    # @skip("Skip test")
    def test_fields(self):
        order = Order.objects.first()
        fields = [field.name for field in order._meta.get_fields()]
        self.assertIn("customer", fields)
        self.assertIn("placement_datetime", fields)
        self.assertIn("delivery_date", fields)
        self.assertIn("status", fields)
        self.assertIn("order_items", fields)

    # @skip("Skip test")
    def test_placement_datetime(self):
        pass

    # @skip("Skip test")
    def test_delivery_date(self):
        pass

    def test_block_status_to_draft(self):
        with factory.Faker.override_default_locale("el_GR"):
            order = FullOrderFactory(order_items=2)

        order.status = OrderStatus.ACTIVE
        order.save()

        self.assertEqual(order.status, OrderStatus.ACTIVE)
        order.status = OrderStatus.DRAFT
        self.assertRaises(ValidationError, order.save)
