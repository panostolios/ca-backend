import json
from django.contrib.auth import get_user_model
from django.urls import reverse
import factory
from rest_framework import status
from rest_framework.test import APITestCase
from unittest import skipIf


from api.models import Customer
from api.serializers import CustomerSerializer
from api.factories import CustomerFactory
from utils import clean_phone


User = get_user_model()
credentials = {"username": "whatever", "password": "secret"}


class TestCustomerListAPI(APITestCase):
    def setUp(self):
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        # Create 10 random customers
        with factory.Faker.override_default_locale("el_GR"):
            CustomerFactory.create_batch(10)
        self.customers = Customer.objects.all()

    # @skipIf(True,"Skip test")
    def test_get_customer_list(self):
        response = self.client.get(reverse("customer-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_data: list = response.json()
        serializer = CustomerSerializer(self.customers, many=True)
        self.assertEqual(len(response_data), len(serializer.data))


class TestCustomerFilters(APITestCase):
    def setUp(self):
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        # Create 10 random customers
        with factory.Faker.override_default_locale("el_GR"):
            CustomerFactory.create_batch(10)
        self.customers = Customer.objects.all()

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_first_name__iexact(self):
        # Test case insensitive first_name filtering
        # Test name with capital first letter
        test_name: str = "Albert"
        with factory.Faker.override_default_locale("el_GR"):
            # create 'albert' customers
            CustomerFactory(first_name="albert")
            CustomerFactory(first_name="Αlbert")
            CustomerFactory(first_name="aLbΕrt")
        albert_customers = Customer.objects.filter(first_name__iexact=test_name)
        response = self.client.get(
            reverse("customer-list"), {"first_name__iexact": test_name}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), albert_customers.count())
            for data in response.data:
                self.assertEqual(data["first_name"].lower(), test_name.lower())

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_first_name__istartswith(self):
        # Test case insensitive first_name startswith filtering
        test_name: str = "Jo"
        with factory.Faker.override_default_locale("el_GR"):
            # create customers with first_name starting from 'Jo'
            CustomerFactory(first_name="Joe")
            CustomerFactory(first_name="john")
            CustomerFactory(first_name="jOjObA")
        jo_customers = Customer.objects.filter(first_name__istartswith=test_name)
        response = self.client.get(
            reverse("customer-list"), {"first_name__istartswith": test_name}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), jo_customers.count())
            for data in response.data:
                self.assertTrue(
                    data["first_name"].lower().startswith(test_name.lower())
                )

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_first_name__icontains(self):
        # Test case insensitive first_name contains filtering
        test_name: str = "li"
        with factory.Faker.override_default_locale("el_GR"):
            # create customers with first_name containing 'Jo'
            CustomerFactory(first_name="Alice")
            CustomerFactory(first_name="alison")
            CustomerFactory(first_name="StaLin")
        li_customers = Customer.objects.filter(first_name__icontains=test_name)
        response = self.client.get(
            reverse("customer-list"), {"first_name__icontains": test_name}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), li_customers.count())
            for data in response.data:
                self.assertIn(test_name.lower(), data["first_name"].lower())

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_last_name__iexact(self):
        # Test case insensitive last_name filtering
        # Test name with capital first letter
        test_name: str = "Einstein"
        with factory.Faker.override_default_locale("el_GR"):
            # create 'Einstein' customers
            CustomerFactory(first_name="Einstein")
            CustomerFactory(first_name="einstein")
            CustomerFactory(first_name="EinStein")
        einstein_customers = Customer.objects.filter(last_name__iexact=test_name)
        response = self.client.get(
            reverse("customer-list"), {"last_name__iexact": test_name}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), einstein_customers.count())
            for data in response.data:
                self.assertEqual(data["last_name"].lower(), test_name.lower())

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_last_name__istartswith(self):
        # Test case insensitive larst_name startswith filtering
        test_name: str = "Da"
        with factory.Faker.override_default_locale("el_GR"):
            # create customers with larst_name starting from 'Da'
            CustomerFactory(last_name="Darwin")
            CustomerFactory(last_name="davidson")
            CustomerFactory(last_name="DaVinci")
        da_customers = Customer.objects.filter(last_name__istartswith=test_name)
        response = self.client.get(
            reverse("customer-list"), {"last_name__istartswith": test_name}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), da_customers.count())
            for data in response.data:
                self.assertTrue(data["last_name"].lower().startswith(test_name.lower()))

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_last_name__icontains(self):
        # Test case insensitive last_name contains filtering
        test_name: str = "li"
        with factory.Faker.override_default_locale("el_GR"):
            # create customers with last_name containing 'li'
            CustomerFactory(last_name="Alison")
            CustomerFactory(last_name="borelli")
            CustomerFactory(last_name="StaLin")
        li_customers = Customer.objects.filter(last_name__icontains=test_name)
        response = self.client.get(
            reverse("customer-list"), {"last_name__icontains": test_name}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), li_customers.count())
            for data in response.data:
                self.assertIn(test_name.lower(), data["last_name"].lower())

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_email__iexact(self):
        # Test case insensitive email filtering
        # Test all-lowercase email
        test_email: str = "a.einstein@genius.edu"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(email="a.einstein@genius.edu")
            CustomerFactory(email="A.einsTein@geniUs.edu")
            CustomerFactory(email="a.einstein@genius.edu".upper())
        customers = Customer.objects.filter(email__iexact=test_email)
        response = self.client.get(
            reverse("customer-list"), {"email__iexact": test_email}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertEqual(data["email"].lower(), test_email.lower())

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_email__istartswith(self):
        # Test case insensitive email startswith filtering
        # Test all-lowercase email
        test_email: str = "dar"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(email="darwincharles@evolut.edu")
            CustomerFactory(email="Darwincharles@evolut.edu")
            CustomerFactory(email="daRwinCharles@Evolut.edu")
        customers = Customer.objects.filter(email__istartswith=test_email)
        response = self.client.get(
            reverse("customer-list"), {"email__istartswith": test_email}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertTrue(data["email"].lower().startswith(test_email))

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_email__icontains(self):
        # Test case insensitive email icontains filtering
        # Test all-lowercase email
        test_email: str = "google"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(email="a.einstein@gooGle.co.uk")
            CustomerFactory(email="darWincharles@google.com")
            CustomerFactory(email="mariE_curie@google.org")
        customers = Customer.objects.filter(email__icontains=test_email)
        response = self.client.get(
            reverse("customer-list"), {"email__icontains": test_email}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertIn(test_email, data["email"].lower())

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_postal_code(self):
        # Test postal_code filtering
        test_postal_code: str = "65498"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory.create_batch(3, postal_code=test_postal_code)
        customers = Customer.objects.filter(postal_code=test_postal_code)
        response = self.client.get(
            reverse("customer-list"), {"postal_code": test_postal_code}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertEqual(data["postal_code"], test_postal_code)

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_postal_code__icontains(self):
        # Test postal_code icontains filtering
        test_postal_code: str = "49"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(postal_code="49365")
            CustomerFactory(postal_code="65495")
            CustomerFactory(postal_code="35649")
        customers = Customer.objects.filter(postal_code__icontains=test_postal_code)
        response = self.client.get(
            reverse("customer-list"), {"postal_code__icontains": test_postal_code}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertIn(test_postal_code, data["postal_code"])

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_country__iexact(self):
        # Test case insensitive country filtering
        # Test all-lowercase country
        test_country: str = "finland"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(country="finland")
            CustomerFactory(country="Finland")
            CustomerFactory(country="fiNlanD")
        customers = Customer.objects.filter(country__iexact=test_country)
        response = self.client.get(
            reverse("customer-list"), {"country__iexact": test_country}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertEqual(data["country"].lower(), test_country)

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_country__istartswith(self):
        # Test case insensitive country startswith filtering
        test_country: str = "mal"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(country="Malta")
            CustomerFactory(country="malaysia")
            CustomerFactory(country="mAlawI")
        customers = Customer.objects.filter(country__istartswith=test_country)
        response = self.client.get(
            reverse("customer-list"), {"country__istartswith": test_country}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertTrue(data["country"].lower().startswith(test_country))

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_country__icontains(self):
        # Test case insensitive country icontains filtering
        test_country: str = "al"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(country="Italy")
            CustomerFactory(country="malta")
            CustomerFactory(country="nepAl")
        customers = Customer.objects.filter(country__icontains=test_country)
        response = self.client.get(
            reverse("customer-list"), {"country__icontains": test_country}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertIn(test_country, data["country"].lower())

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_city__iexact(self):
        # Test case insensitive city filtering
        # Test all-lowercase city
        test_city: str = "london"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(city="london")
            CustomerFactory(city="London")
            CustomerFactory(city="loNDon")
        customers = Customer.objects.filter(city__iexact=test_city)
        response = self.client.get(
            reverse("customer-list"), {"city__iexact": test_city}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertEqual(data["city"].lower(), test_city)

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_city__istartswith(self):
        # Test case insensitive city startswith filtering
        test_city: str = "bel"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(city="Belgrade")
            CustomerFactory(city="belfast")
            CustomerFactory(city="belo Horizonte")
        customers = Customer.objects.filter(city__istartswith=test_city)
        response = self.client.get(
            reverse("customer-list"), {"city__istartswith": test_city}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertTrue(data["city"].lower().startswith(test_city))

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_city__icontains(self):
        # Test case insensitive city icontains filtering
        test_city: str = "lin"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(city="Berlin")
            CustomerFactory(city="dublin")
            CustomerFactory(city="TaLliNn")
        customers = Customer.objects.filter(city__icontains=test_city)
        response = self.client.get(
            reverse("customer-list"), {"city__icontains": test_city}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertIn(test_city, data["city"].lower())

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_tin(self):
        # Test case insensitive city filtering
        # Test all-lowercase city
        test_tin: str = "654987321"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(tin="654987321")
        customers = Customer.objects.filter(tin=test_tin)
        response = self.client.get(reverse("customer-list"), {"tin": test_tin})
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertEqual(data["tin"], test_tin)

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_tin__istartswith(self):
        # Test case insensitive tin istartswith filtering
        test_tin: str = "654987"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(tin="654987")
        customers = Customer.objects.filter(tin__istartswith=test_tin)
        response = self.client.get(
            reverse("customer-list"), {"tin__istartswith": test_tin}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertTrue(data["tin"].startswith(test_tin))

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_tin__icontains(self):
        # Test case insensitive tin icontains filtering
        test_tin: str = "465"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(tin="124655736")
            CustomerFactory(tin="465578913")
            CustomerFactory(tin="454656814")
        customers = Customer.objects.filter(tin__icontains=test_tin)
        response = self.client.get(
            reverse("customer-list"), {"tin__icontains": test_tin}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertIn(test_tin, data["tin"].lower())

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_phone(self):
        # Test phone filtering
        test_phone: str = "654987321"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(phone="+6549(87)321")
            CustomerFactory(phone="65*49 87 3 21")
            CustomerFactory(phone="654-9873-2_1")
        # the following doesn't work, it will return 0 results
        # customers = Customer.objects.filter(phone=test_phone)
        response = self.client.get(reverse("customer-list"), {"phone": test_phone})
        if response.status_code == status.HTTP_200_OK:
            # I had to hard-code the number of customers returned because
            # the Customer manager can't return results based on clean_phone
            self.assertEqual(len(response.data), 3)
            for data in response.data:
                self.assertEqual(clean_phone(data["phone"]), test_phone)

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_phone_contains(self):
        # Test phone contains filtering
        test_phone: str = "5498"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory(phone="+6549(87)321")
            CustomerFactory(phone="65*49 87 3 21")
            CustomerFactory(phone="654-9873-2_1")
        # the following doesn't work, it will return 0 results
        # customers = Customer.objects.filter(phone=test_phone)
        response = self.client.get(
            reverse("customer-list"), {"phone__contains": test_phone}
        )
        if response.status_code == status.HTTP_200_OK:
            # I had to hard-code the number of customers returned because
            # the Customer manager can't return results based on clean_phone
            self.assertEqual(len(response.data), 3)
            for data in response.data:
                self.assertIn(test_phone, clean_phone(data["phone"]))

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_full_name(self):
        test_full_name = "Albert Einstein"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory.create_batch(10)
            CustomerFactory.create_batch(
                3,
                first_name=test_full_name.split()[0],
                last_name=test_full_name.split()[1],
            )
        customers = Customer.objects.filter(fullname=test_full_name)

        # Test existing customer
        response = self.client.get(
            reverse("customer-list"), {"fullname": test_full_name}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertEqual(
                    test_full_name, f"{data['first_name']} {data['last_name']}"
                )

        # Test non-existing customer
        response = self.client.get(reverse("customer-list"), {"fullname": "Isaac"})

        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), 0)

    # @skipIf(True,"Skip test")
    def test_filter_customer_by_full_name_iexact(self):
        test_full_name = "Albert Einstein"
        with factory.Faker.override_default_locale("el_GR"):
            # create test customers
            CustomerFactory.create_batch(10)
            CustomerFactory.create_batch(
                3,
                first_name=test_full_name.split()[0].lower(),
                last_name=test_full_name.split()[1].lower(),
            )
        customers = Customer.objects.filter(fullname__iexact=test_full_name)

        # Test existing customer
        response = self.client.get(
            reverse("customer-list"), {"fullname__iexact": test_full_name}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), customers.count())
            for data in response.data:
                self.assertEqual(
                    test_full_name.lower(), f"{data['first_name']} {data['last_name']}"
                )
        # Test non-existing customer
        response = self.client.get(
            reverse("customer-list"), {"fullname__iexact": "isaac"}
        )
        if response.status_code == status.HTTP_200_OK:
            self.assertEqual(len(response.data), 0)


class TestSingleCustomerAPI(APITestCase):
    def setUp(self):
        User.objects.create_user(**credentials)
        self.client.login(**credentials)
        with factory.Faker.override_default_locale("el_GR"):
            self.einstein_data: dict = factory.build(
                dict,
                FACTORY_CLASS=CustomerFactory,
                first_name="Albert",
                last_name="Einstein",
            )
            self.curie_data: dict = factory.build(
                dict,
                FACTORY_CLASS=CustomerFactory,
                first_name="Marie",
                last_name="Curie",
            )
            self.newton_data: dict = factory.build(
                dict,
                FACTORY_CLASS=CustomerFactory,
                first_name="Isaac",
                last_name="Newton",
            )

        self.einstein = Customer.objects.create(**self.einstein_data)

    # @skipIf(True,"Skip test")
    def test_get_single_valid_customer(self):
        response = self.client.get(
            reverse("customer-detail", kwargs={"pk": self.einstein.pk})
        )
        customer = Customer.objects.get(pk=self.einstein.pk)
        serializer = CustomerSerializer(customer)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # @skipIf(True,"Skip test")
    def test_get_single_invalid_customer(self):
        response = self.client.get(reverse("customer-detail", kwargs={"pk": 1001}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    # @skipIf(True,"Skip test")
    def test_post_single_valid_customer(self):
        curie = json.dumps(self.curie_data)
        response = self.client.post(
            reverse("customer-list"), data=curie, content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    # @skipIf(True,"Skip test")
    def test_post_single_invalid_customer(self):
        # Remove first_name, which is required
        self.curie_data["first_name"] = ""
        curie = json.dumps(self.curie_data)
        response = self.client.post(
            reverse("customer-list"), data=curie, content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Set an invalid email
        self.newton_data["email"] = "dfggsfd"
        newton = json.dumps(self.newton_data)
        response = self.client.post(
            reverse("customer-list"), data=newton, content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # @skipIf(True,"Skip test")
    def test_update_single_valid_customer(self):
        self.einstein_data["first_name"] = "Alexander"
        einstein = json.dumps(self.einstein_data)
        response = self.client.put(
            reverse("customer-detail", kwargs={"pk": self.einstein.pk}),
            data=einstein,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # @skipIf(True,"Skip test")
    def test_update_single_invalid_customer(self):
        # Remove first_name, which is required
        self.einstein_data["first_name"] = ""
        einstein = json.dumps(self.einstein_data)
        response = self.client.put(
            reverse("customer-detail", kwargs={"pk": self.einstein.pk}),
            data=einstein,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # Set an invalid email
        self.einstein_data["first_name"] = "Albert"
        self.einstein_data["email"] = "dfggsfd"
        einstein = json.dumps(self.einstein_data)
        response = self.client.put(
            reverse("customer-detail", kwargs={"pk": self.einstein.pk}),
            data=einstein,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # @skipIf(True,"Skip test")
    def test_delete_valid_single_customer(self):
        curie = Customer.objects.create(**self.curie_data)
        response = self.client.delete(
            reverse("customer-detail", kwargs={"pk": curie.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    # @skipIf(True,"Skip test")
    def test_delete_invalid_single_customer(self):
        response = self.client.delete(reverse("customer-detail", kwargs={"pk": 1001}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
