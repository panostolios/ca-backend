import json
from typing import TYPE_CHECKING

from django.conf import settings
import factory
from rest_framework.test import APITestCase

from api.models import Customer
from api.serializers import CustomerSerializer, MiniCustomerSerializer
from api.factories import CustomerFactory

if TYPE_CHECKING:
    from django.db.models.query import QuerySet


class TestCustomerSerializer(APITestCase):
    def setUp(self):
        self.maxDiff = None
        with factory.Faker.override_default_locale("el_GR"):
            self.customers: list = factory.build_batch(
                dict, 10, FACTORY_CLASS=CustomerFactory
            )
        for customer in self.customers:
            Customer.objects.create(**customer)
            customer["full_name"] = (
                f"{customer['last_name']} {customer['first_name']}".strip()
            )

    def test_serialize_model_instance(self):
        customer = Customer.objects.get(id=1)
        customer_data: dict = self.customers[0]
        customer_data["id"] = 1
        customer_data["code"] = "CU-0000001"
        serializer = CustomerSerializer(customer)
        self.assertJSONEqual(json.dumps(serializer.data), customer_data)

    def test_serialize_two_model_instances(self):
        customers: QuerySet = Customer.objects.filter(id__in=[1, 2])
        customers_json = json.loads(json.dumps(self.customers[:2]))
        customers_json[0]["id"] = 1
        customers_json[0]["code"] = "CU-0000001"
        customers_json[1]["id"] = 2
        customers_json[1]["code"] = "CU-0000002"
        serializer = CustomerSerializer(customers, many=True)
        self.assertJSONEqual(json.dumps(serializer.data), customers_json)

    def test_deserialize_valid_data_create(self):
        customers_json: list = json.loads(json.dumps(self.customers[:2]))
        num_of_items: int = len(customers_json)
        serializer = CustomerSerializer(data=customers_json, many=True)
        count_before: int = Customer.objects.count()
        self.assertTrue(serializer.is_valid())
        serializer.save()
        count_after: int = Customer.objects.count()
        self.assertEqual(count_before + num_of_items, count_after)

    def test_deserialize_invalid_data_create(self):
        customer = self.customers[0]
        customer["first_name"] = ""
        customer["email"] = "invalid email"
        serializer = CustomerSerializer(data=customer)
        self.assertFalse(serializer.is_valid())
        blank_field_error_message: str = (
            "Το πεδίο δε μπορεί να είναι κενό."
            if settings.LANGUAGE_CODE == "el-gr"
            else "This field may not be blank."
        )
        self.assertEqual(serializer.errors["first_name"][0], blank_field_error_message)
        valid_email_error_message: str = (
            "Εισάγετε μια έγκυρη διεύθυνση ηλ. ταχυδρομείου."
            if settings.LANGUAGE_CODE == "el-gr"
            else "Enter a valid email address."
        )
        self.assertEqual(serializer.errors["email"][0], valid_email_error_message)

    def test_deserialize_valid_data_update(self):
        customer = Customer.objects.get(id=1)
        customer_data = self.customers[0]
        customer_data["first_name"] = "Alberto"
        customer_data["last_name"] = "Boticelli"
        serializer = CustomerSerializer(customer, data=customer_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        customer = Customer.objects.get(id=1)
        self.assertEqual(customer.first_name, customer_data["first_name"])
        self.assertEqual(customer.last_name, customer_data["last_name"])


class TestMiniCustomerSerializer(APITestCase):
    def setUp(self):
        self.maxDiff = None
        with factory.Faker.override_default_locale("el_GR"):
            self.customers: list = factory.build_batch(
                dict, 10, FACTORY_CLASS=CustomerFactory
            )
        for customer in self.customers:
            Customer.objects.create(**customer)

    def test_serialize_model_instances(self):
        customers: QuerySet = Customer.objects.filter(id__in=[1, 2])
        customers_json = json.loads(json.dumps(self.customers[:2]))
        customers_json[0]["id"] = 1
        customers_json[1]["id"] = 2
        serializer = MiniCustomerSerializer(customers, many=True)
        for customer, model_customer in zip(serializer.data, customers):
            self.assertEqual(model_customer.full_name, customer["full_name"])
            self.assertEqual(model_customer.id, customer["id"])
