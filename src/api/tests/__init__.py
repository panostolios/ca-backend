# Couldn't discover tests in VS Code
# I found this solution here:
# https://stackoverflow.com/questions/55911159/problem-with-django-app-unit-tests-under-visual-studio-code

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ca_api.settings")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
