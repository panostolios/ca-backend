from rest_framework import serializers

from api.models import Customer


class BaseCustomerSerializer(serializers.ModelSerializer):
    code = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()

    class Meta:
        model = Customer
        fields = "__all__"

    def get_full_name(self, obj):
        return obj.full_name

    def get_code(self, obj):
        return f"CU-{obj.id:07d}"


class CustomerSerializer(BaseCustomerSerializer):
    pass


class MiniCustomerSerializer(BaseCustomerSerializer):
    """This serializer should be used only to retrieve customer data

    It is used in OrderSerializer
    for a "compact" representation of the customer
    """

    class Meta(BaseCustomerSerializer.Meta):
        fields = ("id", "full_name")

    def to_representation(self, instance):
        return {"id": instance.id, "full_name": instance.full_name}
