from .authentication import LoginSerializer
from .customer import CustomerSerializer, MiniCustomerSerializer
from .order import (
    OrderSerializer,
    OriginalOrderSerializer,
    FullOrderSerializer,
    OrderItemSerializer,
    FullOrderItemSerializer,
    PanelGroupSerializer,
)
from .product import ProductSerializer
