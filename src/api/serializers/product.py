from rest_framework import serializers

from api.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"


class MiniMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ("id", "name")
        extra_kwargs = {"name": {"read_only": True}}

    def get_queryset(self):
        return Product.objects.filter(type=Product.ProductType.INDUSTRIAL_TIMBER)
