from decimal import Decimal
from rest_framework import serializers, exceptions
from drf_writable_nested.serializers import WritableNestedModelSerializer
from drf_writable_nested.mixins import NestedCreateMixin, NestedUpdateMixin

from api.models import (
    Customer,
    Order,
    OrderItem,
    OriginalOrder,
    OriginalOrderItem,
    PanelGroup,
    StripPosition,
)
from api.serializers.customer import MiniCustomerSerializer
from api.serializers.product import MiniMaterialSerializer
from api.validations.order_serializer_validations import (
    order_prevent_save_if_no_order_items,
    order_prevent_status_to_draft,
)


class StipPositionSerializer(WritableNestedModelSerializer):
    id = serializers.IntegerField(required=False)
    strip = MiniMaterialSerializer()

    class Meta:
        model = StripPosition
        exclude = ["panel_group"]


class PanelGroupSerializer(WritableNestedModelSerializer):
    id = serializers.IntegerField(required=False)
    strip_positions = StipPositionSerializer(many=True, required=False)

    class Meta:
        model = PanelGroup
        exclude = ["order_item"]


class OrderItemSerializer(WritableNestedModelSerializer):
    order = serializers.PrimaryKeyRelatedField(queryset=Order.objects.all())
    panel_groups = PanelGroupSerializer(many=True)

    class Meta:
        model = OrderItem
        fields = "__all__"


class FullOrderItemSerializer(WritableNestedModelSerializer):
    material = MiniMaterialSerializer()
    panel_groups = PanelGroupSerializer(many=True)

    class Meta:
        model = OrderItem
        exclude = ["order"]


class BaseOrderSerializer(serializers.ModelSerializer):
    code = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = "__all__"

    def get_code(self, obj):
        return f"OR-{obj.id:07d}"


class OrderSerializer(BaseOrderSerializer):
    customer = MiniCustomerSerializer()


class FullOrderSerializer(NestedCreateMixin, NestedUpdateMixin, BaseOrderSerializer):
    customer = serializers.PrimaryKeyRelatedField(queryset=Customer.objects.all())
    order_items = FullOrderItemSerializer(many=True)

    def to_internal_value(self, data):
        if data.get("order_items"):
            for order_item in data["order_items"]:
                for panel_group in order_item["panel_groups"]:
                    if Decimal(panel_group["x_dimension"]) < Decimal(
                        panel_group["y_dimension"]
                    ):
                        panel_group["x_dimension"], panel_group["y_dimension"] = (
                            panel_group["y_dimension"],
                            panel_group["x_dimension"],
                        )

        return super().to_internal_value(data)

    def create(self, validated_data):
        order_prevent_save_if_no_order_items(validated_data)
        return super().create(validated_data)

    def update(self, instance, validated_data):
        order_prevent_status_to_draft(instance, validated_data)
        order_prevent_save_if_no_order_items(validated_data)
        super().update(instance, validated_data)
        return instance


class OriginalPanelGroupSerializer(serializers.ModelSerializer):
    original_strip_positions = StipPositionSerializer(many=True)

    class Meta:
        model = PanelGroup
        exclude = ["order_item"]


class OriginalOrderItemSerializer(serializers.ModelSerializer):
    material = MiniMaterialSerializer()
    original_panel_groups = OriginalPanelGroupSerializer(many=True)

    class Meta:
        model = OriginalOrderItem
        exclude = ["order"]


class OriginalOrderSerializer(BaseOrderSerializer):
    original_order_items = OriginalOrderItemSerializer(many=True)
    code = None

    class Meta:
        model = OriginalOrder
        fields = "__all__"
