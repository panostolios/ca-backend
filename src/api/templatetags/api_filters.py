from decimal import Decimal
from django import template

register = template.Library()


def multiply(value, arg):
    return Decimal(value) * Decimal(arg)


def divide(value, arg):
    return Decimal(value) / Decimal(arg)


def subtract(value, arg):
    return Decimal(value) - Decimal(arg)


register.filter("multiply", multiply)
register.filter("divide", divide)
register.filter("subtract", subtract)
