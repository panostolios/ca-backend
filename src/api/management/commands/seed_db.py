from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
import factory
import random

from api.models import Product, StripPosition
from api.factories import (
    CustomerFactory,
    OrderFactory,
    OrderItemFactory,
    PanelGroupFactory,
    ProductFactory,
)


class Command(BaseCommand):
    help = "Creates sample data in the database"

    def handle(self, *args, **options):
        self.create_products(**options)
        customers = self.create_customers(**options)
        self.create_orders(customers, **options)

    def create_customers(self, **options):
        verbosity = int(options["verbosity"])
        number_of_customers = (
            options["customers_to_create"] if options.get("customers_to_create") else 20
        )
        customers = []
        with factory.Faker.override_default_locale("el_GR"):
            for _ in range(number_of_customers):
                c = CustomerFactory()
                customers.append(c)
                if verbosity == 2:
                    self.stdout.write(f"Created customer with name {c.full_name}")
        if verbosity in (1, 2):
            self.stdout.write(self.style.SUCCESS("Successfully created customers"))
        return customers

    def create_products(self, **options):
        verbosity = int(options["verbosity"])
        number_of_products = (
            options["numbers_to_create"] if options.get("numbers_to_create") else 20
        )
        for _ in range(number_of_products):
            p = ProductFactory(industrial_timber=True)
            if verbosity == 2:
                self.stdout.write(f"Created material product with name {p.name}")

        for _ in range(number_of_products):
            strip_product = ProductFactory(strip=True)
            if verbosity == 2:
                self.stdout.write(
                    f"Created strip product with name {strip_product.name}"
                )

        if verbosity in [1, 2]:
            self.stdout.write(self.style.SUCCESS(f"Succesfully created products"))

    def create_orderitems(self, order):
        number_of_items = random.randint(1, 7)
        products = list(
            Product.objects.filter(type=Product.ProductType.INDUSTRIAL_TIMBER)
        )
        strips = list(Product.objects.filter(type=Product.ProductType.STRIP))
        strip_positions_map = {
            0: [StripPosition.Position.XTOP, StripPosition.Position.XBOTTOM],
            1: [StripPosition.Position.XTOP, StripPosition.Position.YTOP],
            2: [StripPosition.Position.YTOP, StripPosition.Position.YBOTTOM],
        }
        for _ in range(number_of_items):
            order_item = OrderItemFactory(order=order, material=random.choice(products))
            for i in range(number_of_items):
                panel_group = PanelGroupFactory(order_item=order_item)
                if strip_positions_map.get(i):
                    for position in strip_positions_map[i]:
                        try:
                            StripPosition.objects.create(
                                panel_group=panel_group,
                                strip=random.choice(strips),
                                strip_position=position,
                            )
                        except ValidationError:
                            pass

    def create_orders(self, customers, **options):
        verbosity = int(options["verbosity"])
        with factory.Faker.override_default_locale("el_GR"):
            for customer in customers:
                or1 = OrderFactory(customer=customer)
                self.create_orderitems(or1)
                if verbosity == 2:
                    self.stdout.write(f"Created {or1}")
                or2 = OrderFactory(customer=customer)
                self.create_orderitems(or2)
                if verbosity == 2:
                    self.stdout.write(f"Created {or2}")
            if verbosity in (1, 2):
                self.stdout.write(self.style.SUCCESS("Succesfully created orders"))
