from rest_framework import exceptions


def order_prevent_save_if_no_order_items(validated_data):
    if validated_data["status"] > 1 and len(validated_data["order_items"]) == 0:
        raise exceptions.ValidationError(
            "You cannot create an Order with no order items", "invalid"
        )


def order_prevent_status_to_draft(instance, validated_data):
    if validated_data["status"] == 1 and instance.status > 1:
        raise exceptions.ValidationError(
            "You cannot set the Order status back to 'Draft'", "invalid"
        )
