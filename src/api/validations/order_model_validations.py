from ast import List
from copy import deepcopy
from django.core import exceptions
from django.forms import model_to_dict

from api import models


def order_before_create(instance):
    pass


def order_before_update(old_instance, instance):
    order_block_status_to_draft(old_instance, instance)
    pass


def order_after_update(old_instance, instance):
    create_original_order(old_instance, instance)


def order_block_status_to_draft(old_instance, instance):
    # Don't allow to change the Order status back to 'Draft'
    # if has already advanced to another status
    if instance.status == 1 and old_instance.status > 1:
        raise exceptions.ValidationError(
            "You cannot set the Order status back to 'Draft'", "invalid"
        )


def order_block_status_if_no_order_items(instance):
    if instance is None:
        return
    if instance.status > 1 and instance.order_items.count() == 0:
        raise exceptions.ValidationError(
            "You cannot save an Order with no order items",
            "invalid",
        )


def create_original_order(old_instance, instance):
    if not (instance.status == 2 and old_instance.status == 1):
        return

    order_data = deepcopy(model_to_dict(instance))
    order_data["id"] = None
    order_data["order"] = instance
    del order_data["status"]
    order_data["customer"] = instance.customer
    original_order = models.OriginalOrder(**order_data)
    original_order.save()
    original_order_items: List[models.OriginalOrderItem] = []

    for order_item in instance.order_items.all():
        oi = model_to_dict(order_item)
        oi["origin_order_item"] = order_item
        oi["material"] = order_item.material
        oi["order"] = original_order
        del oi["id"]

        original_order_items.append(models.OriginalOrderItem(**oi))

    models.OriginalOrderItem.objects.bulk_create(original_order_items)
    original_order_items_created = models.OriginalOrderItem.objects.filter(
        order=original_order
    )

    original_panel_groups: List[models.OriginalPanelGroup] = []
    for original_order_item in original_order_items_created:
        for panel_group in original_order_item.origin_order_item.panel_groups.all():
            original_panel_group_data = model_to_dict(panel_group)
            del original_panel_group_data["id"]
            original_panel_group_data["order_item"] = original_order_item
            original_panel_group_data["origin_panel_group"] = panel_group
            original_panel_groups.append(
                models.OriginalPanelGroup(**original_panel_group_data)
            )

    models.OriginalPanelGroup.objects.bulk_create(original_panel_groups)

    original_strip_positions: List[models.OriginalStripPosition] = []
    for original_panel_group in models.OriginalPanelGroup.objects.filter(
        order_item__order=original_order
    ):
        for (
            strip_position
        ) in original_panel_group.origin_panel_group.strip_positions.all():
            sp = model_to_dict(strip_position)
            sp["strip"] = strip_position.strip
            del sp["id"]
            sp["panel_group"] = original_panel_group
            original_strip_positions.append(models.OriginalStripPosition(**sp))

    models.OriginalStripPosition.objects.bulk_create(original_strip_positions)
