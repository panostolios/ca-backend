from django_filters import ChoiceFilter
from django_filters import rest_framework as filters

from api.models import Customer, Order, Product
from utils import clean_phone


class CustomerFilter(filters.FilterSet):
    fullname = filters.CharFilter(method="filter_by_fullname")
    fullname__iexact = filters.CharFilter(method="filter_by_fullname")

    def filter_by_fullname(self, queryset, name, value):
        return queryset.filter(**{name: value})

    def filter_queryset(self, queryset):
        match_ids: set = set()
        for customer in self.queryset:
            # if a phone is given as a query string
            if self.data.get("phone") == clean_phone(customer.phone):
                match_ids.add(customer.id)
            # if a phone__contains is given as a query string
            if str(self.data.get("phone__contains")) in clean_phone(customer.phone):
                match_ids.add(customer.id)
        if match_ids:
            qs = Customer.objects.filter(pk__in=match_ids)
            return qs
        return super().filter_queryset(queryset)

    class Meta:
        model = Customer
        fields = {
            "first_name": ["iexact", "istartswith", "icontains"],
            "last_name": ["iexact", "istartswith", "icontains"],
            "email": ["iexact", "istartswith", "icontains"],
            "postal_code": ["exact", "icontains"],
            "country": ["iexact", "istartswith", "icontains"],
            "city": ["iexact", "istartswith", "icontains"],
            "tin": ["exact", "istartswith", "icontains"],
            "phone": ["exact", "contains"],
        }


class OrderFilter(filters.FilterSet):
    class Meta:
        model = Order
        fields = {
            "customer": ["exact"],
            "status": ["exact"],
            "delivery_date": ["exact", "lt", "gt", "lte", "gte"],
            "placement_datetime": ["exact", "lt", "gt", "lte", "gte"],
        }


class ProductFilter(filters.FilterSet):
    category = ChoiceFilter(choices=Product.ProductCategory.choices)
    type = ChoiceFilter(choices=Product.ProductType.choices)
    unit_of_measure = ChoiceFilter(choices=Product.ProductUnitOfMeasure.choices)

    class Meta:
        model = Product
        fields = {
            "name": ["icontains"],
            "category": ["exact"],
            "type": ["exact"],
            "unit_of_measure": ["exact"],
        }
