from django.contrib import admin
from .models import (
    Customer,
    Order,
    OrderItem,
    OriginalOrder,
    OriginalOrderItem,
    PanelGroup,
    Product,
)


class PanelGroupInline(admin.StackedInline):
    model = PanelGroup


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    fields = (
        "id",
        "material",
    )


class OriginalOrderItemInline(admin.TabularInline):
    model = OriginalOrderItem
    fields = (
        "id",
        "material",
    )
    show_change_link = True


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        "first_name",
        "last_name",
        "tax_office",
        "tin",
        "phone",
        "email",
        "street_address",
        "postal_code",
        "city",
        "country",
        "notes",
    )


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ("id", "customer", "placement_datetime", "delivery_date", "status")
    inlines = [OrderItemInline]


@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "order",
    )
    inlines = [PanelGroupInline]


@admin.register(OriginalOrder)
class OriginalOrderAdmin(admin.ModelAdmin):
    list_display = ("id", "order", "customer", "placement_datetime", "delivery_date")
    inlines = [OriginalOrderItemInline]


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass
