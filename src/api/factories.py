import datetime

from django.core.exceptions import ValidationError
import factory
from factory import fuzzy

from api.models import (
    Customer,
    Order,
    OrderItem,
    OrderStatus,
    Product,
    PanelGroup,
    StripPosition,
)
from utils import clean_phone, clean_postal


class CustomerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Customer

    class Params:
        name = factory.Faker("name")
        sentence = factory.Faker("sentence")
        vat_id = factory.Faker("vat_id")
        tax_office_city = factory.Faker("city")
        raw_phone = factory.Faker("phone_number")
        raw_postal = factory.Faker("postcode")

    first_name = factory.LazyAttribute(lambda obj: obj.name.split()[0])
    last_name = factory.LazyAttribute(lambda obj: obj.name.split()[1])
    tin = factory.LazyAttribute(lambda obj: obj.vat_id[2:])
    tax_office = factory.LazyAttribute(lambda obj: obj.tax_office_city)
    phone = factory.LazyAttribute(lambda obj: clean_phone(obj.raw_phone))
    email = factory.Faker("email")
    street_address = factory.Faker("street_address")
    postal_code = factory.LazyAttribute(lambda obj: clean_postal(obj.raw_postal))
    city = factory.Faker("city")
    country = factory.Faker("country")
    notes = factory.LazyAttribute(lambda obj: obj.sentence[:-1])


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product

    class Params:
        sheet_material = fuzzy.FuzzyChoice(choices=["Melamine", "MDF", "Plywood"])
        sheet_color = fuzzy.FuzzyChoice(choices=["White", "Rosewood", "Maple"])
        sheet_thickness = fuzzy.FuzzyChoice(choices=[9, 12, 16, 18, 25, 32])
        industrial_timber = factory.Trait(
            name=factory.LazyAttribute(
                lambda obj: f"{obj.sheet_material} {obj.sheet_color} {obj.sheet_thickness}mm"
            ),
            category=Product.ProductCategory.GOOD,
            type=Product.ProductType.INDUSTRIAL_TIMBER,
            unit_of_measure=Product.ProductUnitOfMeasure.SQM,
        )
        strip = factory.Trait(
            name=factory.LazyAttribute(
                lambda obj: f"Strip {obj.sheet_color} {obj.sheet_thickness}mm"
            ),
            category=Product.ProductCategory.GOOD,
            type=Product.ProductType.STRIP,
            unit_of_measure=Product.ProductUnitOfMeasure.M,
        )


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order

    class Params:
        placement = fuzzy.FuzzyDateTime(
            datetime.datetime(2018, 1, 1, tzinfo=datetime.timezone.utc),
            datetime.datetime.now(tz=datetime.timezone.utc),
        )
        random_days = fuzzy.FuzzyInteger(1, 90)
        # NOTE: https://gist.github.com/rg3915/db907d7455a4949dbe69#file-gen_random_datetime-py

    customer = factory.SubFactory(CustomerFactory)
    placement_datetime = factory.LazyAttribute(lambda obj: obj.placement)
    delivery_date = factory.LazyAttribute(
        lambda obj: (obj.placement + datetime.timedelta(days=obj.random_days)).date()
    )
    status = OrderStatus.DRAFT


class FullOrderFactory(OrderFactory):
    @factory.post_generation
    def order_items(obj, create, extracted, **kwargs):
        """
        If called like: FullOrderFactory(order_items=4) it generates an Order with 4
        order_items.  If called without `order_items` argument, it generates a
        random amount of order_items for this order
        """
        if not create:
            # Build, not create related
            return

        if extracted:
            for _ in range(extracted):
                OrderItemFactory(order=obj)
        else:
            import random

            number_of_units = random.randint(1, 10)
            for _ in range(number_of_units):
                OrderItemFactory(order=obj)


# NOTE: https://simpleit.rocks/python/django/setting-up-a-factory-for-one-to-many-relationships-in-factoryboy/#strategies


class OrderItemFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OrderItem

    class Params:
        x_dimension_decimal = fuzzy.FuzzyDecimal(10, 1000, 1)
        y_dimension_decimal = fuzzy.FuzzyDecimal(10, 1000, 1)

    order = factory.SubFactory(OrderFactory)
    material = factory.SubFactory(ProductFactory, industrial_timber=True)

    @factory.post_generation
    def panel_groups(obj, create, extracted, **kwargs):
        import random

        """
        If called like: FullOrderFactory(order_items=4) it generates an Order with 4
        order_items.  If called without `order_items` argument, it generates a
        random amount of order_items for this order
        """
        if not create:
            # Build, not create related
            return

        number_of_units = extracted if extracted else random.randint(1, 10)

        for _ in range(number_of_units):
            PanelGroupFactory(order_item=obj)


class PanelGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PanelGroup

    order_item = factory.SubFactory(OrderItemFactory)
    quantity = fuzzy.FuzzyInteger(1, 100)
    x_dimension = fuzzy.FuzzyDecimal(10, 1000, 1)
    y_dimension = fuzzy.FuzzyDecimal(10, 1000, 1)

    @factory.post_generation
    def strip_positions(obj, create, extracted, **kwargs):
        import random

        if not create:
            # Build, not create related
            return

        number_of_units = extracted if extracted else random.randint(1, 4)

        for _ in range(number_of_units):
            try:
                StripPositionFactory(panel_group=obj)
            except ValidationError:
                pass


class StripPositionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = StripPosition

    panel_group = factory.SubFactory(PanelGroup)
    strip = factory.SubFactory(ProductFactory, strip=True)

    strip_position = fuzzy.FuzzyChoice(choices=StripPosition.Position)
