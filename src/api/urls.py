from django.urls import path
from rest_framework.routers import DefaultRouter

from api import views

router = DefaultRouter()
router.register("customers", views.CustomerViewSet)
router.register("orders", views.OrderViewSet)
router.register("order-items", views.OrderItemViewSet)
router.register("products", views.ProductViewSet)

urlpatterns = [
    path(
        "order-statuses", views.ListOrderStatusesView.as_view(), name="order-statuses"
    ),
    path("orders/<int:id>/pdf", views.PDFView.as_view()),
    path(
        "orders/<int:pk>/original_order",
        views.OriginalOrderViewSet.as_view({"get": "retrieve"}),
        name="original-order",
    ),
    path("login", views.LoginView.as_view()),
    path("logout", views.LogoutView.as_view()),
]

urlpatterns += router.urls
