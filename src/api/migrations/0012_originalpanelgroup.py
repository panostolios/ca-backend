# Generated by Django 3.2.22 on 2024-03-04 10:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0011_originalorderitem'),
    ]

    operations = [
        migrations.CreateModel(
            name='OriginalPanelGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveSmallIntegerField()),
                ('x_dimension', models.DecimalField(decimal_places=1, max_digits=5)),
                ('y_dimension', models.DecimalField(decimal_places=1, max_digits=5)),
                ('order_item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='original_panel_groups', to='api.orderitem')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
